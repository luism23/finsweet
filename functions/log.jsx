const log = (name,data,color="white") => {
    console.log(`%c [${name.toLocaleUpperCase()}]`, `color:${color};`, data);
}

export default log