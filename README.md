# Template for Nextjs

It is a template for Nextjs, for used in Startscoinc, used NextJs and Sass

# Install

Open terminal and clone repository [https://gitlab.com/franciscoblancojn/templatenextjs.git](https://gitlab.com/franciscoblancojn/templatenextjs.git)

```bash
git clone https://gitlab.com/franciscoblancojn/templatenextjs.git 
```

Rename Forlder

```bash
mv templatenextjs nameNewProyect
```

Move in Forlder

```bash
cd nameNewProyect
```

Reset git
```bash
rm -fr .git
git init
```

Add your remote repositoy Git

```bash
git remote add origin <server>
```

Add your commit

```bash
git add .
git commit -m "Initial Commit"
```

Push in your Repository

```bash
git push
```

Install dependencies

```bash
npm i
```

Run dev

```bash
npm run dev
```

# Structure

## api
It is forder for connect nextjs with your api

## components
It is forder for create your components for proyect

## data
It is forder for save information static for pages

## functions
It is forder for create your funcions for proyect

## pages
It is forder for create your pages for proyect

## server
It is forder for create your getServerSideProps and getStaticProps for proyect

## Styles
It is forder for save your style and style static for proyect

### Font Example Use
| Property Css  | Component Jsx                                 | Value Css                 |
| ---           | ---                                           |---                        |
| font-size     | ```<h1 className="font-20">Text</h1>```       | (20px / 16px * 1rem)      |
| font-weight   | ```<h1 className="font-w-700">Text</h1>```    | 700                       |
| line-height   | ```<h1 className="line-h-15">Text</h1>```     | 1.5                       |

### Heght Example Use
| Property Css  | Component Jsx                                 | Value Css                 |
| ---           | ---                                           |---                        |
| height        | ```<h1 className="height-20">Text</h1>```     | (20px / 16px * 1rem)      |
| height        | ```<h1 className="height-vh-20">Text</h1>```  | 20vh                      |
| height        | ```<h1 className="height-p-20">Text</h1>```   | 20%                       |

## svg
It is forder for save your svg for proyect


## Developer

[Francisco Blanco](https://franciscoblanco.vercel.app/)