import Arrow from "@/svg/arrow"

export default {

    questions: {
        title: "Frequently asked questions",
        link: "/",
        textLink: "Contact us for more info",
        contentInfo: [
            {
                title: "How much time does it take?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "What is your class naming convention?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "How do you communicate?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "I have a bigger project. Can you handle it?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "What is your class naming convention?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
        ]
    },
    plansPrincing: {
        title: "Our Pricing Plans",
        text: "When you’re ready to go beyond prototyping in Figma, Webflow is ready to help you bring your designs to life — without coding them.",
        conentInfo: [
            {
                textNumber: "$299",
                text1: "Per Design",
                title: "Landing Page ",
                text2: "When you’re ready to go beyond prototyping in Figma, ",
                linksSpam: [
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Own analytics platform
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Chat support
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Optimize hashtags
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Unlimited users
                            </>
                        ),
                    },
                ],
            },
            {
                textNumber: "$399",
                text1: "Multi Design",
                title: "Website Page ",
                text2: "When you’re ready to go beyond prototyping in Figma, Webflow’s ready to help. ",
                linksSpam: [
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Own analytics platform
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Chat support
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Optimize hashtags
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Unlimited users
                            </>
                        ),
                    },
                ],
            },
            {
                textNumber: "$499 +",
                text1: "Per Design",
                title: "Landing Page ",
                text2: "When you’re ready to go beyond prototyping in Figma, ",
                linksSpam: [
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                All limited links
                            </>
                        ),
                    },
                    {
                        text: (
                            <>
                                <Arrow size={25} className="m-r-10" />
                                Assist and Help
                            </>
                        ),
                    },
                ],
            }
        ],

    }



}