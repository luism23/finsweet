export default {

    banner: {
        title: "All the features you need",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        linkInput: [
            {
                link: "/",
                text: "View our work",
                img: "",
                color: "bg-yellow-1  color-black ",
            },
            {
                link: "/",
                color: "bg-transparent color-white ",
            }
        ],
        img: "img-banner.png",
    },
    imglink: {
        imgLinks: [
            {
                link: "/",
                img: "Logo-1.png",
            },
            {
                link: "/",
                img: "Logo-2.png",
            },
            {
                link: "/",
                img: "Logo-3.png",
            },
            {
                link: "/",
                img: "Logo-4.png",
            },
            {
                link: "/",
                img: "Logo-5.png",
            }
        ],
    },
    benefits2: {
        title: "The benefits of working with us",
        contentInfo: [
            {
                img: "Icon-6.png",
                title: "Customize with ease",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim.",
            },
            {
                img: "Icon-3.png",
                title: "Perfectly Responsive",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim.",
            },
            {
                img: "Icon-1.png",
                title: "Friendly Support",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim.",
            },
        ],

    },
    missionVision: {
        our: [
            {
                subTitle: "Use Client-first",
                title: "Top agencies and freelancers around the world use Client-first ",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                img: "img1.png",
            },
            {
                subTitle: "Free Revision Rounds ",
                title: "Get free Revisions and one week of free maintenance",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
                img: "img2.png"
            },
            {
                subTitle: "24/7 Support",
                title: "Working with us, you will be getting 24/7 priority support",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
                img: "img3.png",
            },
            {
                subTitle: "Quick Delivery",
                title: "Guranteed 1 week delivery for standard five pager website",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                img: "img4.png",
            },

        ]
    },
    questions: {
        title: "Frequently asked questions",
        link: "/",
        textLink: "Contact us for more info",
        contentInfo: [
            {
                title: "How much time does it take?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "What is your class naming convention?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "How do you communicate?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "I have a bigger project. Can you handle it?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "What is your class naming convention?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
        ]
    },
}