import Arrow from "@/svg/arrow"

export default {
    blog:{
        title:"A UX Case Study on Creating a Studious Environment for Students",
        text1:"Andrew Jonson Posted on 27th January 2021",
        img:"blog.png",
        text2:"Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon of the sea to the level of the spectator on a hillside.",
        link:"/",
        linkText:"Read More"

    },
    ourBlog: {
        title: "Our blog",
        contentInfo: [
            {
                img: "our1.png",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/1",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            },
            {
                img: "our2.png",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/2",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            },
            {
                img: "our3.png",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/3",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            },
            {
                img: "our4.png",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/1",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            },
            {
                img: "our5.png",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/2",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            },
            {
                img: "our6.png",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/3",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            }
        ]
    }
}