export default {
    about: {
        subTitle: "About us",
        title: "Our designs solve problems",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
        img: "abouts-img.jpeg",
    },
    infoText: {
        subTitle: "Who we are",
        title: "Goal focussed",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        title2: "Continuous improvement",
        text2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    },
    fondoImg: {
        img: "fondoImg.jpeg",
    },
    follow: {
        title: "The process we follow",
        contentInfo: [
            {
                img: "circulo.png",
                title: "Planning",
                text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
            },
            {
                img: "circulo.png",
                title: "Conception",
                text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
            },
            {
                img: "circulo.png",
                title: "Design",
                text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
            },
            {
                img: "circulo.png",
                title: "Development",
                text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
            },
        ],
    },
    missionVision: {
        our: [

            {
                subTitle: "Our Mission ",
                title: "Inspire, Innovate, Share",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                img: "men.png"
            },
            {
                subTitle: "Our Vision",
                title: "Laser focus",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                img: "woman.png",
            }
        ]
    },
    benefits: {
        title: "The benefits of working with us",
        contentInfo: [
            {
                img: "Icon-6.png",
                title: "Customize with ease",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim.",
            },
            {
                img: "Icon-3.png",
                title: "Perfectly Responsive",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim.",
            },
            {
                img: "Icon-1.png",
                title: "Friendly Support",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim.",
            },
        ],
        textNumber:"100.000+",
        text:"Finsweet Users",
        imgLinks:[
            {
                link:"/",
                img:"Logo-1.png",
            },
            {
                link:"/",
                img:"Logo-2.png",
            },
            {
                link:"/",
                img:"Logo-3.png",
            },
            {
                link:"/",
                img:"Logo-4.png",
            },
            {
                link:"/",
                img:"Logo-5.png",
            }
        ],
    },
    meet:{
        title:"Meet our team",
        contentMeet:[
            {
                img:"foto1.png",
                link:"/",
                linkText:"John Smith",
                text:"CEO",
            },
            {
                img:"foto2.png",
                link:"/",
                linkText:"Simon Adams",
                text:"CTO",
            },
            {
                img:"foto3.png",
                link:"/",
                linkText:"Paul Jones",
                text:"Design Lead",
            },
            {
                img:"foto4.png",
                link:"/",
                linkText:"Sara Hardin",
                text:"Project Manager",
            },
        ],
    },
}