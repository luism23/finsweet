
import Arrow from "@/svg/arrow"

export default {
    workPortfolio: {
        subTitle: "What we created",
        title: "Our Work Portfolio",
        text: "We help teams create great digital products by providing them with tools and technology to make the design-to-code process universally accessible.",
        linkRs: [
            {
                link: "/",
                img: "face.png",
            },
            {
                link: "/",
                img: "twi.png",
            },
            {
                link: "/",
                img: "ins.png",
            },
            {
                link: "/",
                img: "in.png",
            },
        ]
    },
    template: {
        templateLink: [
            {
                link: "/",
                linkText: "All",
            },
            {
                link: "/",
                linkText: "UI Design",
            },
            {
                link: "/",
                linkText: "Webflow Design",
            },
            {
                link: "/",
                linkText: "Figma Design",
            },
        ],
        templateInfo: [
            {
                img: "work1.png",
                title: "Template 1",
                text: "Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle.",
                link: "/",
                linkText: (
                    <>
                        View Portfolio
                        <Arrow size={25} className="m-l-10" />
                    </>
                ) 
            },
            {
                img: "work2.png",
                title: "Template 2",
                text: "Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle.",
                link: "/",
                linkText: (
                    <>
                        View Portfolio
                        <Arrow size={25} className="m-l-10" />
                    </>
                ) 
            },
            {
                img: "work3.png",
                title: "Template 3",
                text: "Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle.",
                link: "/",
                linkText: (
                    <>
                        View Portfolio
                        <Arrow size={25} className="m-l-10" />
                    </>
                ) 
            },
            {
                img: "work4.png",
                title: "Template 4",
                text: "Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle.",
                link: "/",
                linkText: (
                    <>
                        View Portfolio
                        <Arrow size={25} className="m-l-10" />
                    </>
                )   
            },
            {
                img: "work5.png",
                title: "Template 5",
                text: "Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle.",
                link: "/",
                linkText: (
                    <>
                        View Portfolio
                        <Arrow size={25} className="m-l-10" />
                    </>
                )   
            },
            {
                img: "work6.png",
                title: "Template 6",
                text: "Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle.",
                link: "/",
                linkText: (
                    <>
                        View Portfolio
                        <Arrow size={25} className="m-l-10" />
                    </>
                )   
            },
        ]
    },
    letsBuild:{
        title:"Let's build something great together",
        text:"Nullam vitae purus at tortor mattis dapibus. Morbi purus est, ultricies nec dolor sit amet, scelerisque cursus purus.",
        link:"/contact",
        linkText:"Contact Us",
    }
}