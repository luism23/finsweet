import Arrow from "@/svg/arrow"

export default {
    banner: {
        title: "Building stellar websites for early startups",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.",
        linkInput: [
            {
                link: "/work",
                text: "View our work",
                color: "bg-yellow-1  color-black ",
            },
            {
                link: "/pricing",
                color: "bg-transparent color-white ",
                text: (
                    <>
                        View Pricing
                        <Arrow size={25} className="m-l-10" />


                    </>
                ),

            }
        ],
        img: "img-banner.png",
    },
    work: {
        title: "How we work",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
        link: "/work",
        textLink: (
            <>
                Get in touch with us
                <Arrow size={25} className="m-l-10" />
            </>
        ),
        workInfo: [
            {
                img: "1.png",
                title: "Strategy",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam .",
            },
            {
                img: "2.png",
                title: "Wireframing",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam .",
            },
            {
                img: "3.png",
                title: "Design",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam .",
            },
            {
                img: "4.png",
                title: "Development",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam .",
            },
        ],
    },
    viewProyects: {
        title: "View our projects",
        imgFondo1: "fondo-proyects.png",
        titleInfo1: "Workhub office Webflow Webflow Design",
        text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam ",
        link2: "/work",
        textLink2: (
            <>
                View proyects
                <Arrow size={25} className="m-l-10" />
            </>
        ),
        imgFondo2: "fondo-proyects-1.png",
        titleInfo2: "Unisaas Website Design",
        link3: "/work",
        textLink3: (
            <>
                View portfolio
                <Arrow size={25} className="m-l-10" />
            </>
        ),
        img: "fondo-proyects-2.png",
    },
    problemsProducts: {
        title: "Design that solves problems, one product at a time",
        problemsProducts: [
            {
                img: "Icon-1.png",
                title: "Uses Client First",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam sed faucib turpis eu gravida mi. Pellentesque et velit aliquam sed mi. ",
            },
            {
                img: "Icon-2.png",
                title: "Two Free Revision Round",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam sed faucib turpis eu gravida mi. Pellentesque et velit aliquam sed mi. ",
            },
            {
                img: "Icon-3.png",
                title: "Template Customization",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam sed faucib turpis eu gravida mi. Pellentesque et velit aliquam sed mi. ",
            },
            {
                img: "Icon-4.png",
                title: "24/7 Support",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam sed faucib turpis eu gravida mi. Pellentesque et velit aliquam sed mi. ",
            },
            {
                img: "Icon-5.png",
                title: "Quick Delivery",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam sed faucib turpis eu gravida mi. Pellentesque et velit aliquam sed mi. ",
            },
            {
                img: "Icon-6.png",
                title: "Hands-on approach",
                text: "Euismod faucibus turpis eu gravida mi. Pellentesque et velit aliquam sed faucib turpis eu gravida mi. Pellentesque et velit aliquam sed mi. ",
            },
        ]
    },
    clients: {
        title: "What our clients say about us",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit sed.",
        textInfo: "The best agency we’ve worked with so far. They understand our product and are able to add new features with a great focus.",
        img: "user.png",
        textUser: "Jenny Wilson",
        subTextUser: "Vice President",
    },
    questions: {
        title: "Frequently asked questions",
        link: "/contact",
        textLink: "Contact us for more info",
        contentInfo: [
            {
                title: "How much time does it take?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "What is your class naming convention?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "How do you communicate?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "I have a bigger project. Can you handle it?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
            {
                title: "What is your class naming convention?",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            },
        ]
    },
    login: {
        img: "fondo.png",
        title: "Building stellar websites for early startups",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim.",
        titleInfo: "Send inquiry",
        textInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",
        link: "/contact",
        textLink: (
            <>
                Get in touch with us
                <Arrow size={25} className="m-l-10" />
            </>

        ),
    },
    ourBlog: {
        title: "Our blog",
        contentInfo: [
            {
                img: "foto-1.jpg",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/1",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            },
            {
                img: "foto-2.jpg",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/2",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            },
            {
                img: "foto-3.jpg",
                subTitle: "19 Jan 2022",
                title: "How one Webflow user grew his single person consultancy from $0-100K in 14 months",
                text: "See how pivoting to Webflow changed one person’s sales strategy and allowed him to attract",
                link: "/blog/3",
                textLink: (
                    <>
                        Read More
                        <Arrow size={25} className="m-l-10" />
                    </>
                ),
            }
        ]
    }


}