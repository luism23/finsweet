export default {
    img: "logo.png",
    linkTitle: [
        {
            link: "/",
            title: "Home",
        },
        {
            link: "/about-us",
            title: "About us",
        },
        {
            link: "/features",
            title: "Features",
        },
        {
            link: "/pricing",
            title: "Pricing",
        },
        {
            link: "/blog",
            title: "Blog",
        },
    ],
    linkInput: [
        {
            link: "/contact",
            text: "Contact us",
        },

    ],
}