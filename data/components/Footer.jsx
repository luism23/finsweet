export default {
    img: "logo.png",
    text: "We are always open to discuss your project and improve your online presence.",
    linkDirection: [
        {
            title: "Email me at",
            link: "/",
            textLink: "contact@website.com",
        },
        {
            title: "Call us",
            link: "/",
            textLink: "0927 6277 28525",
        },
    ],
    title: "Lets Talk!",
    text1: "We are always open to discuss your project, improve your online presence and help with your UX/UI design challenges.",
    linkRs: [
        {
            link: "/",
            img: "facebook.png",
        },
        {
            link: "/",
            img: "facebook.png",
        },
        {
            link: "/",
            img: "facebook.png",
        },
        {
            link: "/",
            img: "facebook.png",
        },
        {
            link: "/",
            img: "facebook.png",
        },
    ],
    textFooter: "Copyright 2022, Finsweet.com",
    linkTitle: [
        {
            link: "/",
            title: "Home",
        },
        {
            link: "/",
            title: "About us",
        },
        {
            link: "/",
            title: "Features",
        },
        {
            link: "/pricing",
            title: "Pricing",
        },
        {
            link: "/",
            title: "Blog",
        },
        {
            link: "/privacy",
            title: "Privacy Policy",
        },
    ],

}