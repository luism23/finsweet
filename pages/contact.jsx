import Content from "@/components/Content"
import Contact from "@/components/contact"


import Info from "@/data/pages/contact"

const Index = () => {
    return (
        <>
            <Content>
                <Contact {...Info.contact} />
              
            </Content>
        </>
    )
}
export default Index