import { ToastContainer} from 'react-toastify';

import '../styles/global.css'
import '../styles/style.css'

function MyApp({ Component, pageProps }) {
  return(<>
     <Component {...pageProps} />
     <ToastContainer />
  </>)
}

export default MyApp
