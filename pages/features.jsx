import Content from "@/components/Content"
import Banner from "@/components/banner"
import ImgLink from "@/components/imgLink"
import Benefits2 from "@/components/benefits2"
import MissionVision from "@/components/missionVision"
import Questions from "@/components/questions"

import Info from "@/data/pages/features"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner} />
                <ImgLink {...Info.imglink} />
                <Benefits2 {...Info.benefits2} />
                <MissionVision {...Info.missionVision} />
                <Questions {...Info.questions} />
                
            </Content>
        </>
    )
}
export default Index