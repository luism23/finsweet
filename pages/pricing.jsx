import Content from "@/components/Content"
import Questions from "@/components/questions"
import PlansPrincing from "@/components/plansPrincing"


import Info from "@/data/pages/princing"

const Index = () => {
    return (
        <>
            <Content>
                <PlansPrincing {...Info.plansPrincing} />
                <Questions {...Info.questions} />
            </Content>
        </>
    )
}
export default Index