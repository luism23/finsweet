import Content from "@/components/Content"
import WorkPortfolio from "@/components/workPortfolio"
import Template from "@/components/template"
import LetsBuild from "@/components/letsBuild"


import Info from "@/data/pages/work"

const Index = () => {
    return (
        <>
            <Content>
                <WorkPortfolio {...Info.workPortfolio} /> 
                <Template {...Info.template} />  
                <LetsBuild {...Info.letsBuild} />
            </Content>
        </>
    )
}
export default Index