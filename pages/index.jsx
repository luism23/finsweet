import Content from "@/components/Content"
import Banner from "@/components/banner"
import Work from "@/components/work"
import ViewProyects from "@/components/viewProyects"
import ProblemsProducts from "@/components/problemsProducts"
import Clients from "@/components/clients"
import Questions from "@/components/questions"
import Login from "@/components/login"
import OurBlog from "@/components/ourBlog"

import Info from "@/data/pages/index"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner} />
                <Work {...Info.work} />
                <ViewProyects {...Info.viewProyects} />
                <ProblemsProducts {...Info.problemsProducts} />
                <Clients {...Info.clients} />
                <Questions {...Info.questions} />
                <Login {...Info.login} />
                <OurBlog {...Info.ourBlog} />
            </Content>
        </>
    )
}
export default Index