import Content from "@/components/Content"
import Blog from "@/components/blog"
import OurBlog from "@/components/ourBlog"


import Info from "@/data/pages/blog"

const Index = () => {
    return (
        <>
            <Content>
                <Blog {...Info.blog} />
                <OurBlog {...Info.ourBlog} />
            </Content>
        </>
    )
}
export default Index