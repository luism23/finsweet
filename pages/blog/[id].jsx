import Content from "@/components/Content"
import Blog from "@/components/blog"
import TitleText from "@/components/titleText"
import BlogContentInfo from "@/components/blogContentInfo"

import getBlog from "@/api/getBlog"

import log from "@/functions/log"

const Index = ({Info}) => {
    log("Info",{Info},"yellow")
    if(Info == "" || Info === undefined){
        return(
            <>
                <Content>
                    <h1 className="m-v-150">
                        Blog no encontado
                    </h1>
                </Content>
            </>
        )
    }
    return (
        <>
            <Content>
                <Blog {...Info.blog} />
                <TitleText {...Info.titleText} />
                <BlogContentInfo {...Info.blogContentInfo} />
            </Content>
        </>
    )
}
export async function getServerSideProps({ params }) {
    const id = parseFloat(params.id)
    const Info = await getBlog(id -1 )
    return {
        props: {
            Info
        },
    };
}


export default Index