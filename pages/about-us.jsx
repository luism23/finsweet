import Content from "@/components/Content"
import About from "@/components/about"
import InfoText from "@/components/infoText"
import FondoImg from "@/components/fondoImg"
import Follow from "@/components/follow"
import MissionVision from "@/components/missionVision"
import Benefits from "@/components/benefits"
import Meet from "@/components/meet"

import Info from "@/data/pages/about"

const Index = () => {
    return (
        <>
            <Content>
                <About {...Info.about} />
                <InfoText {...Info.infoText} />
                <FondoImg {...Info.fondoImg} />
                <Follow {...Info.follow} />
                <MissionVision {...Info.missionVision} />
                <Benefits {...Info.benefits} />
                <Meet {...Info.meet} />
            </Content>
        </>
    )
}
export default Index