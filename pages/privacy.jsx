import Content from "@/components/Content"
import Privacy from "@/components/privacy"


import Info from "@/data/pages/privacy"

const Index = () => {
    return (
        <>
            <Content>
                <Privacy {...Info.privacy} /> 
            </Content>
        </>
    )
}
export default Index