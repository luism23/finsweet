import request from "@/api/request";


export default async (id) => {
    const result = await request(
        {
            url:`http://localhost:3000/api/blogs?id=${id}`,
            headers: {
                'content-type': 'application/json',
                'Accept': 'application/json',
            },
            type:"get"
        }
    )
    return result
}