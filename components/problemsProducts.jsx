
const ProblemsProducts = ({ title = "", text = "", img = "" }) => {
    return (
        <>
            <div className="flex-12 flex-sm-6 flex-md-4 bg-white flex-sm-gap-32 m-b-24">
                <div>
                    <img className="flex p-t-58 p-l-48 p-b-22" src={`/image/${img}`} alt="" />
                </div>
                <div className="p-h-48">
                    <h3 className="p-b-12 color-blue-2 font-25 font-w-700 font-montserrat">
                        {title}
                    </h3>
                    <p className="font-18 color-blue-2 font-montserrat p-b-48">
                        {text}
                    </p>
                </div>
            </div>

        </>
    )
}


const Index = ({ title = "", problemsProducts = [] }) => {
    return (
        <>
            <div className="bg-gray m-v-128 p-v-40">
                <div>
                    <h1 className="color-blue-2 flex flex-justify-center font-montserrat font-w-700 font-35 m-b-47">
                        {title}
                    </h1>
                </div>
                <div className="container p-h-15 flex flex-sm-gap-32 flex-justify-between">
                    {
                        problemsProducts.map((e, i) => {
                            return (
                                <ProblemsProducts
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>

            </div>

        </>
    )
}
export default Index