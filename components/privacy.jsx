
const ListText = ({ text = "" }) => {
    return (
        <>
            <li className="m-b-12">
                <span className="color-blue-1 font-montserrat font-18">{text}</span> 
            </li>
        </>
    )
}


const ContentInfo = ({ title = "", text = "" }) => {
    return (
        <>
            <div className="">
                <h2 className="color-blue-2 font-montserrat font-w-700 font-30 m-b-32">
                    {title}
                </h2>
                <p className="color-blue-2 font-montserrat  font-18 m-b-40">
                    {text}
                </p>
                
            </div>
        </>
    )
}

const Index = ({ title = "", text1 = "",text2="" ,contentInfo = [],listText=[] }) => {
    return (
        <>
            <div className="container p-h-15 m-v-200">
                <div className="text-center">
                    <h1 className="color-blue-2 font-montserrat font-w-700 font-38 m-b-16">
                        {title}
                    </h1>
                    <p className="color-blue-2 font-montserrat font-18 m-b-64"> 
                        {text1}
                    </p>
                </div>
                <div>
                    <div>
                        {
                            contentInfo.map((e, i) => {
                                return (
                                    <ContentInfo
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                    <ul className="m-b-24 " style={{listStyle:"none"}}>
                        {
                            listText.map((e,i) => {
                                return(
                                    <ListText
                                    key={i}
                                    {...e}
                                    />
                                )
                            })
                        }
                    </ul>
                    <p className="color-blue-1 font-montserrat font-18">
                        {text2}
                    </p>
                </div>
            </div>
        </>
    )
}
export default Index