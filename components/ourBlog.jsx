import Link from "next/link"

const ContentInfo = ({ link = "", text = "", textLink = "", img = "", title = "", subTitle = "" }) => {
    return (
        <>
            <div className="flex-12 flex-sm-4 flex-sm-gap-32 m-t-128">
                <div className="">
                    <img className="" src={`/image/${img}`} alt="" />
                </div>
                <div>
                    <h5 className="m-b-16 color-blue-2 font-montserrat font-12">
                        {subTitle}
                    </h5>
                    <h3 className="m-b-16 color-blue-2 font-montserrat font-25 font-w-500 ">
                        {title}
                    </h3>
                    <p className="m-b-32 color-blue-2 font-montserrat font-18">
                        {text}
                    </p>
                    <div>
                        <Link href={link}>
                            <a className="color-blue-2 font-montserrat font-12">
                                {textLink}
                            </a>

                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}


const Index = ({ title = "", contentInfo = [] }) => {
    return (
        <>
            <div className="container p-h-15">
                <div className="m-b-64 text-center">
                    <h1 className="color-blue-2 font-montserrat font-w-700 font-45">{title}</h1>
                </div>
                <div className="flex  flex-justify-between  flex-sm-gap-32 m-b-20">
                    {
                        contentInfo.map((e, i) => {
                            return (
                                <ContentInfo
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    );
};
export default Index;
