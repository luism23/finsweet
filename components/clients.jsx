const Index = ({title="",text="",textInfo="",textUser="",subTextUser="",img=""}) => {
    return (
        <>
            <div className="flex container p-h-15 m-v-138 flex-justify-between flex-md-gap-50">
                <div className="flex-12 flex-md-4 flex-md-gap-50 m-b-30 m-md-b-0">
                    <h2 className="color-blue-2 font-montserrat font-w-700 font-30 m-b-18">
                        {title}
                    </h2>
                    <p className="color-blue-2 font-montserrat font-16">
                        {text}
                    </p>
                </div>
                <div className="flex-12 flex-md-8 flex-md-gap-50">
                    <div>
                        <p className="color-blue-2 m-b-48 font-montserrat font-w-700 font-35">
                            {textInfo}
                        </p>
                        <div className="flex">
                            <div className="m-r-14">
                                <img className="" src={`/image/${img}`} alt="" />
                            </div>
                            <div className=" flex flex-column p-auto flex-justify-center">
                                <h4 className="font-montserrat font-18 color-blue-2">
                                    {textUser}
                                </h4>
                                <h6 className="font-montserrat font-12 color-blue-2">
                                    {subTextUser}
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index