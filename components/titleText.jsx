
const ListText = ({ text = "" }) => {
    return (
        <>
            <li className="m-b-12">
                <span className="color-blue-1 font-montserrat font-18">{text}</span> 
            </li>
        </>
    )
}


const ContentInfo = ({ title = "", text = "" }) => {
    return (
        <>
            <div className="">
                <h2 className="color-blue-2 font-montserrat font-w-700 font-30 m-b-32">
                    {title}
                </h2>
                <p className="color-blue-2 font-montserrat  font-18 m-b-40">
                    {text}
                </p>
                
            </div>
        </>
    )
}

const Index = ({contentInfo = [],listText=[] }) => {
    return (
        <>
            <div className="container p-h-15 flex flex-justify-center">
                <div className="flex-12 flex-lg-8">
                    <div>
                        {
                            contentInfo.map((e, i) => {
                                return (
                                    <ContentInfo
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                    <ul className="m-b-24 p-l-17" style={{listStyle:"disc"}}>
                        {
                            listText.map((e,i) => {
                                return(
                                    <ListText
                                    key={i}
                                    {...e}
                                    />
                                )
                            })
                        }
                    </ul>
                   
                </div>
            </div>
        </>
    )
}
export default Index