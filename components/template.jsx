import Link from "next/link"

const TemplateInfo = ({ title = "", text = "", link = "", linkText = "",img="" }) => {
    return (
        <>
            <div className="flex-6 flex-gap-30 m-b-64">
                <div>
                    <img className="flex m-b-24 " src={`/image/${img}`} alt="" />
                </div>
                <div>
                    <h2 className="color-blue-2 m-b-12 font-montserrat font-28 font-700">
                        {title}
                    </h2>
                    <p className="color-blue-2 m-b-24 font-montserrat font-18 ">
                        {text}
                    </p>
                    <div>
                        <Link href={link}>
                            <a className="color-blue-2 font-montserrat font-18">{linkText}</a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}

const TemplateLink = ({ link = "", linkText="" }) => {
    return (
        <>
            <div className="flex">
                <Link href={link}>
                    <a className="flex color-blue-2 font-montserrat font-18 font-700 m-r-32 m-b-48">{linkText}</a>
                </Link>

            </div>
        </>
    )
}



const Index = ({ templateInfo = [], templateLink = [] }) => {
    return (
        <>
            <div className="container">
                <div className="flex-justify-center flex">
                    {
                        templateLink.map((e, i) => {
                            return (
                                <TemplateLink
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
                <div className="">
                    <div className="flex-gap-30 flex-justify-between flex">
                        {
                            templateInfo.map((e, i) => {
                                return (
                                    <TemplateInfo
                                        key={i}
                                        {...e}
                                    />
                                )
                            })

                        }
                    </div>

                </div>
            </div>
        </>
    )
}
export default Index