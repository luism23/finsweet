import Link from "next/link";

import Img from "@/components/Img"


const Index = ({ title = "", text1 = "", img = "", text2 = "", link = "", linkText = "" }) => {
    return (
        <>
            <div className="container p-h-15 m-t-200 ">
                <div className="flex flex-justify-center text-center">
                    <div className="flex-12 flex-md-8">
                        <h1 className="m-b-16 color-blue-2 font-40 font-w-700 font-montserrat">{title}</h1>
                        <p className="m-b-32 color-blue-2 font-16 font-montserrat">
                            {text1}
                        </p>
                    </div>
                </div>
                <Img
                    src={img}
                    classNameImg="m-b-48"
                />

                <div className="text-center flex flex-justify-center">
                    <div className="flex-12 flex-md-8">
                        <p className="m-b-16 color-blue-2 font-18 font-montserrat">
                            {text2}
                        </p>
                        <Link href={link}>
                            <a className="color-purple font-18 font-w-500 font-montserrat">{linkText}</a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    );
};
export default Index;
