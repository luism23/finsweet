import Link from "next/link"


const ImgLinks = ({ link = "", img = "" }) => {
    return (
        <>
            <div>
                <Link href={link}>
                    <img className="flex p-v" src={`/image/${img}`} alt="" />
                </Link>
            </div>
        </>
    )
}


const ContentInfo = ({ img = "", title = "", text = "", }) => {
    return (
        <>
            <div className="flex-gap-32 flex-12 flex-md-4 p-v-48 p-l-48 p-r-32 bg-gray-1">
                <div>
                    <img className="flex m-b-26" src={`/image/${img}`} alt="" />
                </div>
                <div className="">
                    <h4 className="m-b-12 color-blue-2 font montserrat font-25">
                        {title}
                    </h4>
                    <p className=" color-blue-2 font montserrat font-18">
                        {text}
                    </p>
                </div>
            </div>
        </>
    )
}

const Index = ({ contentInfo = [], imgLinks =[], title = "", text = "", textNumber = "" }) => {
    return (
        <>

            <div className="container flex-justify-center flex p-h-15">
                <h1 className="color-blue-2 font montserrat font-45 font-w-700 m-b-48">
                    {title}
                </h1>
            </div>
            <div className="container flex">

                <div className="flex">
                    <div className="flex-gap-32 flex flex-justify-between m-b-48 ">
                        {
                            contentInfo.map((e, i) => {
                                return (
                                    <ContentInfo
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
                <div className="flex container p-h-15 m-b-128">
                    <div className="flex-12 flex-lg-2 m-b-30 ">
                        <h3 className="color-blue-2 font montserrat font-25 m-b-20 m-md-b-0">
                            {textNumber}
                        </h3>
                        <h6 className="color-blue-2 font montserrat font-16">
                            {text}
                        </h6>
                    </div>
                    <div className="flex-12  flex-lg-10 flex flex-justify-between">
                        {
                            imgLinks.map((e, i) => {
                                return (
                                    <ImgLinks
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index