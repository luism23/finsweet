import Link from "next/link"



const WorkInfo = ({  text = "",img="" ,title=""}) => {
    return (
        <>
            <div className="flex-6 flex m-b-48">
                <div className="m-b-16 flex">
                    <img className="flex" src={`/image/${img}`} alt="" />
                </div>
                <div className="flex">
                    <h3 className="font-25 font-w-700 font-poppins color-blue-1 m-b-9 flex">
                        {title}
                    </h3>
                    <p className="font-18 font-poppins color-blue-1">
                        {text}
                    </p>
                </div>

            </div>
        </>
    )
}

const Index = ({ title, text = "", link = "", textLink = "", workInfo = [] }) => {
    return (
        <>
            <div className="m-t-128 m-b-128 ">
                <div className="container p-h-15 flex">
                    <div className="flex-12 flex-md-4 m-b-30 m-md-b-0 ">
                        <h1 className="font-30 font-w-700 font-poppins color-blue-1 m-b-16 ">
                            {title}
                        </h1>
                        <p className="font-18 font-poppins color-blue-1 m-b-16">
                            {text}
                        </p>
                        <div>
                            <Link href={link}>
                                <a className="color-purple font-montserrat font-18">{textLink}</a>
                            </Link>
                        </div>
                    </div>
                    <div className="flex-12 flex-md-8 flex ">
                       <div className="flex">
                       {
                            workInfo.map((e, i) => {
                                return (
                                    <WorkInfo
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                       </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index