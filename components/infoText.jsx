const Index = ({ subTitle = "", title = "", text = "", title2 = "", text2 = "" }) => {
    return (
        <>
            <div className=" flex ">
                <div className="container p-h-15 p-lg-h-94 bg-gray-1 p-v-80  flex flex-md-gap-50 flex-justify-between">
                    <div className="container" >
                        <h6 className=" font-16 font-montserrat  color-blue-1 m-b-8">
                            {subTitle}
                        </h6>
                    </div>
                    <div className="flex-12 flex-md-6 flex-md-gap-50 m-b-20 m-md-b-0">
                        <div className="">

                            <h2 className="font-35 font-montserrat font-w-700 color-blue-1 m-b-31">
                                {title}
                            </h2>
                            <p className="font-218 font-montserrat color-blue-1">
                                {text}
                            </p>
                        </div>
                    </div>
                    <div className="flex-12 flex-md-6 flex-md-gap-50 ">
                        <div className="">
                            <h2 className="font-35 font-montserrat font-w-700 color-blue-1 m-b-31">
                                {title2}
                            </h2>
                            <p className="font-218 font-montserrat color-blue-1">
                                {text2}
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )
}
export default Index