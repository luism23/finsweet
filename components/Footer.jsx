import Link from "next/link"


import Data from "@/data/components/Footer"

const LinkTitle = ({ title = "", link = "" }) => {
    return (
        <>
            <div className="">
                <Link href={link}>
                    <a className="font-16 font-poppins font-w-500  color-black m-r-32 ">
                        {title}
                    </a>
                </Link>
            </div>
        </>
    )
}

const LinkRs = ({ img = "", link = "" }) => {
    return (
        <>
            <div className="flex">
                <Link href={link}>
                    <a><img className="flex m-r-26" src={`/image/${img}`} alt="" /></a>
                </Link>
            </div>
        </>
    )
}

const LinkDirection = ({ title = "", link = "", textLink = "" }) => {
    return (
        <>
            <div className="bg-yellow-1 m-b-16 m-r-29 p-l-37 p-t-18 p-b-16 ">
                <h4 className="">
                    {title}
                </h4>
                <Link href={link}>
                    <a className="font-16 font-poppins font-w-500  color-black ">
                        {textLink}
                    </a>
                </Link>
            </div>
        </>
    )
}


const Index = () => {
    const { text, linkDirection, title, img, text1, linkRs, textFooter, linkTitle } = Data
    return (
        <>
            <div className="bg-blue-1 ">
                <div className="container flex p-h-15  m-b-32">
                    <div className="m-t-100 flex-justify-between flex-md-gap-20 flex">
                        <div className="flex-12 flex-md-6 flex-gap-20 m-md-b-0 m-b-20 ">
                            <img className="flex m-b-40 " src={`/image/${img}`} alt="" />
                            <p className="m-b-16 font-18 font-poppins color-white">
                                {text}
                            </p>
                            <div className="flex bg-yellow-1 flex-justify-between ">
                                {
                                    linkDirection.map((e, i) => {
                                        return (
                                            <LinkDirection
                                                key={i}
                                                {...e}
                                            />

                                        )


                                    })
                                }
                            </div>
                        </div>
                        <div className="flex-12 flex-md-6 flex-md-gap-20 m-md-b-0 m-b-20">
                            <h3 className="m-b-14 font-35 font-poppins font-w-700  color-white">
                                {title}
                            </h3>
                            <p className="m-b-28 font-18 font-poppins   color-white">
                                {text1}
                            </p>
                            <div className="flex ">
                                <div className="flex">
                                {
                                    linkRs.map((e, i) => {
                                        return (
                                            <LinkRs
                                                key={i}
                                                {...e}
                                            />
                                        )

                                    })
                                }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div className="flex container p-h-15 m-v-32">
                    <span className="m-r-auto font-montserrat font-18 color-blue-1 width-p-min-100 width-sm-p-min-0 m-b-20 m-sm-b-0">
                        {textFooter}
                    </span>
                    <div className="flex">
                        {
                            linkTitle.map((e, i) => {
                                return (
                                    <LinkTitle
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
        </>
    )
}
export default Index