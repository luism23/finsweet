import Link from "next/link"


const LinkRs = ({ img = "", link = "" }) => {
    return (
        <>
            <div className="flex">
                <Link href={link}>
                    <a><img className="flex m-r-26" src={`/image/${img}`} alt="" /></a>
                </Link>
            </div>
        </>
    )
}


const Index = ({linkRs="",text="",subTitle="",title=""}) => {
    return (
        <>
            <div className="container m-v-200 bg-gray-2">
                <div className="flex-6 text-center flex-justify-center">
                    <h3 className="color-blue-2 font-16 font-montserrat m-b-4">
                        {subTitle}
                    </h3>
                    <h1 className="color-blue-2 font-45 font-w-700 font-montserrat m-b-24">
                        {title}
                    </h1>
                    <p className="font-18 color-blue-2 font-montserrat m-b-32">
                        {text}
                    </p>
                </div>
                <div className="flex text-center flex-justify-center">
                    {
                        linkRs.map((e, i) => {
                            return (
                                <LinkRs
                                    key={i}
                                    {...e}
                                />
                            )

                        })
                    }
                </div>
            </div>
        </>
    )
}
export default Index