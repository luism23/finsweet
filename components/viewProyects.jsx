import Link from "next/link"

import Bg from "@/components/Bg"



const Index = ({ title = "", link1 = "", textLink1 = "", link2 = "", textLink3 = "", link3 = "", textLink2 = "", img = "", titleInfo1 = "", titleInfo2 = "", text = "", imgFondo1 = "", imgFondo2 = "" }) => {
    return (
        <>
            <div className="container p-h-15">
                <div className="flex flex-justify-between flex-align-center m-b-64">
                    <h1 className="flex font-35 font-montserrat font-w-700 color-blue-1">{title}</h1>
                </div>
                <div className="flex m-b-128">
                    <div className="flex-12 flex-lg-8 pos-r flex">
                        <Bg
                            src={imgFondo1}
                            className=""
                        />
                        <div className="flex width-p-100 height-vh-min-50">
                            <div className="flex-6 flex  bg-gradient-1 flex-column flex-justify-right height-vh-min-50">
                                <div className="p-h-15 p-b-75">
                                    <h3 className="font-30 font-w-700 font-poppins color-white m-b-16">
                                        {titleInfo1}
                                    </h3>
                                    <p className="font-18 color-white font-poppins m-b-40">
                                        {text}
                                    </p>
                                    <Link href={link2}>
                                        <a className="color-yellow-1 font-18 font-poppins">{textLink2}</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-12 flex-lg-4">
                        <div className="pos-r flex-12 flex-md-6 flex-lg-12">
                            <Bg
                                src={imgFondo2}
                                className=""
                            />
                            <div className="p-h-15  p-b-45 p-t-125 bg-gradient-1 flex-column flex-justify-right  height-vh-min-25 ">
                                <h3 className="font-30 font-w-700 font-poppins color-white m-b-16">
                                    {titleInfo2}
                                </h3>

                                <Link href={link3}>
                                    <a className="color-yellow-1 font-18 font-poppins">{textLink3}</a>
                                </Link>
                            </div>
                        </div>
                        <div className="flex-12 flex-md-6 flex-lg-12">
                            <img className="flex height-vh-min-25 m-h-auto" src={`/image/${img}`} alt="" />
                        </div>

                    </div>
                </div>

            </div>
        </>
    )
}
export default Index