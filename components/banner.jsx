import Link from "next/link"

import Img from "@/components/Img"

const LinkInput = ({ link = "", text = "", img = "" ,color=""}) => {
    return (
        <>
            <div className="flex ">
                <Link href={link}>
                    <a className={`width-230 flex flex-justify-center font-montserrat font-18 font-w-500  border-radius-50 flex   p-v-16 p-h-h ${color}`}>
                        {text}
                    </a>

                </Link>
            </div>
        </>
    )
}


const Index = ({ img = "", title = "", text = "", linkInput = [] }) => {
    return (
        <>
            <div className="flex  bg-blue-1">
                <div className="flex container p-h-15 flex-align-center m-t-200 m-md-t-128 m-b-128">
                    <div className="flex-12 flex-md-6 flex">
                        <h1 className="flex font-w-700 font-montserrat font-50 color-white m-b-24 ">
                            {title}
                        </h1>
                        <p className="font-18 font-montserrat color-white m-b-48">
                            {text}
                        </p>
                        <div className="flex">
                            {
                                linkInput.map((e, i) => {
                                    return (
                                        <LinkInput
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>
                    <div className="flex-12 flex-md-6 flex flex-justify-center">
                        <Img
                            src={img}
                            
                            classNameImg="width-max-290 width-md-p-max-100 m-t-50 m-md-t-0"
                        />
                    </div>
                </div>
            </div>

        </>
    );
};
export default Index;
