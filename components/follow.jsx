
const ContentInfo = ({ img = "", title = "", text = "" }) => {
    return (
        <>
            <div className="  flex-12 flex-sm-6 flex-md-3 flex-sm-gap-50 m-b-128">
                <div className="">
                    <img className="flex m-b-32" src={`/image/${img}`} alt="" />
                </div>
                <div className="">
                    <h3 className=" font-22 color-blue-1 font-montserrat m-b-8">
                        {title}
                    </h3>
                    <p className=" font-18 color-blue-1 font-montserrat">
                        {text}
                    </p>
                </div>

            </div>

        </>
    )
}


const Index = ({ title = "", contentInfo = [] }) => {
    return (
        <>
            <div className=" flex-justify-center p-h-15 container ">
                <div className="text-center ">
                    <div className="font-35 color-blue-1 font-w-700 font-montserrat m-b-48">
                        {title}
                    </div>
                </div>
                <div className="container p-h-15">
                    <div className="flex-sm-gap-50 flex flex-justify-between ">
                        {
                            contentInfo.map((e, i) => {
                                return (
                                    <ContentInfo
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                </div>

            </div>

        </>
    )
}
export default Index