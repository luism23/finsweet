import Link from "next/link"
import { useState } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Image from "@/components/Image"

import * as yup from 'yup';
const schema = yup.object().shape({
    name: yup.string().required(),
    email: yup.string().email().required(),
    text: yup.string().required(),
});
const Index = ({ img = "", title = "", text = "", titleInfo = "", textInfo = "", link = "", textLink = "" }) => {
    const [data, setData] = useState({
        name: "",
        email: "",
        text: "",
    })
    const updateData = (id) => (value) => {
        if (value.target) {
            value = value.target.value
        }
        setData({
            ...data,
            [id]: value
        })
    }
    const sendData = (e) => {
        e.preventDefault()
        schema
            .validate(data)
            .then(function (valid) {
                if (valid) {
                    console.log("ok");
                    //aqui
                    setData({
                        name: "",
                        email: "",
                        text: "",
                    })
                }
            }).catch(function (error) {
                console.log(error);
                toast.error(error.message)
            });
    }
    return (
        <>
            <div className="flex container m-b-128 ">
                <div className="flex-12 flex-sm-6 pos-r">
                    <div className="bg">
                        <Image
                            src={img}
                            className="bg-img"
                        />
                        <div className="capa" style={{
                            background: "var(--blue-1)", opacity: .5
                        }}>

                        </div>
                    </div>
                    <div className="flex p-h-15 p-lg-h-96 p-b-261 p-t-96">
                        <h1 className="color-white font-montserrat font-w-700 font-45 m-b-34">
                            {title}
                        </h1>
                        <p className="color-white font-montserrat  font-18">
                            {text}
                        </p>
                    </div>
                </div>
                <div className="flex-12 flex-sm-6 bg-blue-1">
                    <div className="p-h-15 p-lg-h-96 p-t-96 p-b-48">
                        <h2 className="color-white font-montserrat font-w-500 font-25 m-b-16">
                            {titleInfo}
                        </h2>
                        <p className="m-b-40 color-white font-montserrat font-18 ">{textInfo}</p>

                        <div className="m-b-39">
                            <form className="">
                                <input
                                    type="text"
                                    placeholder="Your Name"
                                    className="width-p-100 whidth-432 border-radius-8  border-style-solid border-1 bg-blue-1 login-input color-gray font-18 font-montserrat p-v-18 p-l-32 m-b-16 boder-white-2"
                                    value={data.name}
                                    onChange={updateData("name")}
                                />
                                <input
                                    type="email"
                                    placeholder="Email"
                                    className=" width-p-100 border-radius-8 bg-blue-1 border-style-solid border-2 boder-white-2 login-input color-gray font-18  m-b-16 font-montserrat p-v-18 p-l-32"
                                    value={data.email}
                                    onChange={updateData("email")}
                                />
                                <input
                                    type="url"
                                    placeholder="Paste your Figma design URL"
                                    className="width-p-100 m-b-16 border-radius-8 bg-blue-1 border-style-solid border-1 login-input color-gray font-18 font-montserrat p-v-18 p-l-32 boder-white-2"
                                    value={data.text}
                                    onChange={updateData("text")}
                                />
                                <input
                                    type="submit"
                                    value="Send an Inquiry"
                                    className="  p-v-16 width-p-100 font-montserrat font-20 color-blue-1  bg-yellow-1 flex-justify-center border-radius-50 border-0"
                                    onClick={sendData}
                                />
                            </form>
                        </div>
                        <div className="width-p-100 text-center">
                            <Link href={link}>
                                <a className=" font-montserrat font-20 color-blue-1 bg-blue-1 color-gray  flex-justify-center">{textLink}</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index