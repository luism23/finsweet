import Link from "next/link"

const ContentMeet = ({ link = "", linkText = "", img = "", text = "" }) => {
    return (
        <>
            <div className=" bg-white flex-12 flex-md-6 flex-lg-3 flex flex-md-gap-32 p-t-48 p-b-32 p-h-48 flex-justify-center ">
                <div>
                    <img className="flex m-b-32" src={`/image/${img}`} alt="" />
                </div>
                <div className="width-p-100 text-center">
                    <Link href={link}>
                        <a className="font-w-700 font-25 font-montserrat color-blue-1">{linkText}</a>
                    </Link>
                    <h5 className=" font-18 font-montserrat color-blue-1 m-t-5  text-center">
                        {text}
                    </h5>
                </div>
            </div>

        </>
    )
}

const Index = ({ contentMeet = [], title = "" }) => {
    return (
        <>
            <div className="bg-gray-1 p-v-128">
                <div className="container p-h-15">
                    <div className=" flex flex-justify-center ">
                        <h1 className="m-b-48 color-blue-1 font-45 font-w-700 font-montserrat">
                            {title}
                        </h1>
                    </div>
                    <div className="  flex flex-justify-between flex-md-gap-32">
                        {
                            contentMeet.map((e, i) => {
                                return (
                                    <ContentMeet
                                        key={i}
                                        {...e}
                                    />
                                )
                            })

                        }
                    </div>
                </div>
            </div>

        </>
    )
}
export default Index