import Link from "next/link"

const ContentInfo = ({ title, text,i,length }) => {
    return (
        <>
            <li>
                <div className="flex flex-nowrap width-p-100 m-b-48 m-t-32">
                    <span className="width-100 p-t-4">
                        {i + 1}
                    </span>
                    <div className="width-p-100">
                        <h3 className="color-blue-2 font-montserrat font-22">{title}</h3>
                        <p className="color-blue-2 font-montserrat font-18">
                            {text}
                        </p>
                    </div>
                </div>
                {
                    length != (i +1) &&
                    <hr className="m-0"/>

                }
            </li>
        </>
    )
}

const Index = ({ title="", link = "", textLink = "", titleInfo = "", textInfo = "", contentInfo = [] }) => {
    return (
        <>
            <div className="flex container p-h-15 flex-justify-between flex-md-gap-50 m-t-220">
                <div className="flex-12 flex-md-4 flex-md-gap-50">
                    <h1 className="color-blue-2 font-48 font-w-700 font-montserrat m-b-25">
                        {title}
                    </h1>
                    <Link href={link}>
                        <a className="color-purple font-w-700 font-18 font-montserrat ">{textLink}</a>
                    </Link>
                </div>
                <div className=" flex-12 flex-md-8 flex-md-gap-50">
                    <ul className="color-blue-1" style={{listStyle:"none"}}>
                        {
                            contentInfo.map((e, i) => {
                                return (
                                    <ContentInfo
                                        key={i}
                                        i={i}
                                        length={contentInfo.length}
                                        {...e}
                                    />
                                )

                            })
                        }
                    </ul>
                </div>
            </div>
        </>
    )
}
export default Index