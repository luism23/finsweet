const Index = ({ src = '', name = '', className = '', classNameImg = '', capas = []}) => {
  return (
    <>
      <picture className={className}>
        <source srcSet={`/img/1920/${src}`} media="(min-width: 1680px)" />
        <source srcSet={`/img/1680/${src}`} media="(min-width: 1440px)" />
        <source srcSet={`/img/1440/${src}`} media="(min-width: 1024px)" />
        <source srcSet={`/img/1024/${src}`} media="(min-width: 992px)" />
        <source srcSet={`/img/768/${src}`} media="(min-width: 575px)" />
        <img src={`/img/575/${src}`} alt={name} className={classNameImg} />
        {
          capas.map((e,i)=><div className="capa" style={e} key={i}/>)
        }
      </picture>
    </>
  );
};
export default Index;
