const Index = ({ title = "", text = "", img = "", }) => {
    return (
        <>
            <div className="container p-h-15  m-b-120 flex flex-justify-center">
                <div className="flex-12 flex-lg-8">
                    <div className="">
                        <img className="flex m-b-26 width-p-100" src={`/image/${img}`} alt="" />
                    </div>
                    <div className="">
                        <h1 className="color-blue-2 font-montserrat font-w-700 font-28 m-b-16">{title}</h1>
                        <p className="color-blue-2 font-montserrat font-18">
                            {text}
                        </p>
                    </div>
                </div>
            </div>
        </>
    );
};
export default Index;
