


const Index = ({ title = "", img = "", subTitle = "", text = "" }) => {
    return (
        <>
            <div className="container p-h-15 flex m-b-128 m-t-230 flex-md-gap-50 flex-justify-between">
                <div className="flex-12 flex-md-7 flex-md-gap-50 m-b-30 m-md-b-0  ">
                    <h6 className="font-20 font-montserrat font-w-700 color-blue-1 m-b-8">
                        {subTitle}
                    </h6>
                    <h2 className="font-35 font-montserrat font-w-700 color-blue-1 m-b-31">
                        {title}
                    </h2>
                    <p className="font-218 font-montserrat color-blue-1">
                        {text}
                    </p>
                </div>
                <div className="flex-12 flex-md-5 flex-md-gap-50 ">
                    <img className="width-md-p-max-100 width-max-500 width-p-100" src={`/image/${img}`} alt="" />
                </div>
            </div>
        </>
    )
}
export default Index