import Link from "next/link"

const ImgLinks = ({ link = "", img = "" }) => {
    return (
        <>
            <div className="">
                <Link href={link}>
                    <img className="flex " src={`/image/${img}`} alt="" />
                </Link>
            </div>
        </>
    )
}

const Index = ({imgLinks=[]}) => {
    return (
        <>
            <div className="flex m-v-96">
                <div className="flex container p-h-15 flex-justify-between  ">
                    {
                        imgLinks.map((e, i) => {
                            return (
                                <ImgLinks
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}
export default Index