import Img from "@/components/Img"


const Index = ({img=""}) => {
    return (
        <>
            <div className="m-b-128 container p-h-15">
                <Img
                src={img}
                classNameImg="height-304 width-p-100"
                />
            </div>
        </>
    )
}
export default Index