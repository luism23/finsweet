import Link from "next/link"

const Index = ({ title = "", text = "", link = "", linkText = "" }) => {
    return (
        <>
            <div className="flex-6 flex-justify-center text-center m-v-128">
                <h1 className=" font-montserrat font-45 color-blue-2 font-w-700 m-b-20">
                    {title}
                </h1>
                <p className=" font-montserrat font-18 color-blue-2 m-b-40 ">
                    {text}
                </p>
                <div className="">
                    <Link href={link}>
                        <a className="p-h-66 p-v-16 width-p-100 font-montserrat font-20 color-blue-2  bg-yellow-1 flex-justify-center border-radius-50 border-0">{linkText}</a>
                    </Link>
                </div>
            </div>
        </>
    )
}
export default Index