import Link from "next/link"

const LinksSpam = ({ text = "" }) => {
    return (
        <>
            <li className="m-b-20">
                <span className="m-b-78 color-blue-1 font-montserrat  font-18 ">
                    {text}
                </span>
            </li>
        </>
    )
}


const ConentInfo = ({ textNumber = "", text1 = "", title = "", text2 = "", linksSpam = [] }) => {
    return (
        <>
            <div className="border-radius-12 card-plans flex-12 flex-md-4 bg-gray p-t-64 p-l-48 p-r-77 m-b-20 flex flex-column flex-nowrap flex-md-gap-32">
                <div className="m-b-auto">
                    <div className="flex m-b-20">
                        <h4 className="m-r-8 color-blue-1 font-montserrat font-w-700 font-28">
                            {textNumber}
                        </h4>
                        <h6 className="m-b-8 p-t-3 color-purple font-montserrat font-12 flex flex-align-center">
                            {text1}
                        </h6>
                    </div>
                    <div>
                        <h3 className="m-b-8 color-blue-1 font-montserrat font-w-500 font-25">
                            {title}
                        </h3>
                        <p className=" m-b-50 color-blue-1 font-montserrat font-18">
                            {text2}
                        </p>
                    </div>
                    <div className="m-b-78">
                        <ul className="p-0" style={{ listStyle: "none" }}>
                            {
                                linksSpam.map((e, i) => {
                                    return (
                                        <LinksSpam
                                            key={i}
                                            {...e}
                                        />
                                    )

                                })
                            }
                        </ul>
                    </div>
                </div>
                <div className="p-b-62">
                    <input className="card-plans-btn p-v-16 width-p-100 font-montserrat font-20 color-white bg-blue-1 flex-justify-center border-radius-50 border-0" type="submit" value="Get started" />
                </div>
            </div>
        </>
    )
}



const Index = ({ title = "", text = "", conentInfo = [] }) => {
    return (
        <>
            <div className="container p-h-15">
                <div className="flex flex-justify-center">
                    <div className="text-center flex-6">
                        <h1 className="color-blue-1 font-45 font-w-700 font-montserrat m-b-16">
                            {title}
                        </h1>
                        <p className="color-blue-1 font-18 font-montserrat m-b-64 width-ma">
                            {text}
                        </p>
                    </div>
                </div>
                <div className="flex-md-gap-32 flex-justify-between flex">
                    {
                        conentInfo.map((e, i) => {
                            return (
                                <ConentInfo
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}
export default Index