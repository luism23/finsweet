import { useState } from 'react';

import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import * as yup from 'yup';
const schema = yup.object().shape({
    name: yup.string().required(),
    email: yup.string().email().required(),
    text: yup.string().required(),
    select: yup.string().required(),
    message: yup.string().required(),
});

const Index = ({ title = "", text = "" }) => {
    const [data, setData] = useState({
        name: "",
        email: "",
        text: "",
        select: "",
        message: "",
    })
    const updateData = (id) => (value) => {
        if (value.target) {
            value = value.target.value
           
        }
        setData({
            ...data,
            [id]: value
        })
    }
    const sendData = () => {
        schema
            .validate(data)
            .then(function (valid) {
                if (valid) {
                    console.log("ok");
                }
            }).catch(function (error) {
                console.log(error);
                toast.error(error.message)
            });
    }
    return (
        <>
            <div className="container p-h-15 m-t-200 m-b-128 ">
                <div>
                    <div className="text-center">
                        <h1 className="color-blue-1 font-40 font-w-700 font-montserrat m-b-16">
                            {title}
                        </h1>
                        <p className="color-blue-1 font-18 font-montserrat m-b-64">
                            {text}
                        </p>
                    </div>
                    <div className="bg-gray p-v-60 p-h-92">
                        <div className="flex flex-justify-between flex-gap-32">
                            <div className="flex-12 flex-md-6 flex-gap-32">
                                <label className="font-18 font-montserrat d-block m-b-5">Name</label>
                                <input
                                    className="m-b-24 width-p-100 contact-input font-16 font-montserrat p-v-18 p-l-32"
                                    placeholder="Enter your name"
                                    type="text"
                                    value={data.name}
                                    onChange={updateData("name")}
                                />
                                <label className="font-18 font-montserrat d-block m-b-5">Subject</label>
                                <input
                                    className="m-b-24 width-p-100 contact-input font-16 font-montserrat p-v-18 p-l-32"
                                    placeholder="Provide context"
                                    type="text"
                                    value={data.text}
                                    onChange={updateData("text")}
                                />
                                <label className="font-18 font-montserrat d-block m-b-5">Message</label>
                                <input
                                    className="m-b-24 width-p-100 contact-input font-16 font-montserrat p-v-18 p-l-32"
                                    placeholder="Write your  question here"
                                    type="text"
                                    value={data.message}
                                    onChange={updateData("message")}
                                />
                            </div>
                            <div className="flex-12 flex-md-6 flex-gap-32">
                                <label className="font-18 font-montserrat d-block m-b-5">Email</label>
                                <input
                                    className="m-b-24 width-p-100 contact-input font-16 font-montserrat p-v-18 p-l-32"
                                    placeholder="Enter your Emial"
                                    type="email"
                                    value={data.email}
                                    onChange={updateData("email")}

                                />
                                <label className="font-18 font-montserrat d-block m-b-5">Subject</label>
                                <select
                                    className="m-b-24 width-p-100 contact-input font-16 font-montserrat p-v-18 p-l-32"
                                    name="Select Subject"
                                    id="cars"
                                    value={data.select}
                                    onChange={updateData("select")}
                                >
                                    <option defaultValue disabled value="">Select</option>
                                    <option value="volvo">Subject1</option>
                                    <option value="saab">Subject2</option>
                                    <option value="mercedes">Subject3</option>
                                    <option value="audi">Subject4</option>
                                </select>
                            </div>
                        </div>

                        <div className="text-center m-t-40 ">
                            <input 
                            className="contact-input font-16 font-montserrat p-v-16 p-h-90 border-radius-41 border-style-solid bg-blue-1 color-white  border-0" value="Send Messege"
                            type="submit" 
                            onClick={sendData} />
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}
export default Index